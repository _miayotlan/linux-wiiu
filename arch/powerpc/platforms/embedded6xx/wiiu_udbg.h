/*
 * arch/powerpc/plaforms/wiiu_udbg.c
 *
 * Udbg backend to send messages over Wii U's IPC
 * Copyright (C) 2017 Ash Logan <quarktheawesome@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 */

#ifndef __WIIU_UDBG_H
#define __WIIU_UDBG_H

void udbg_init_wiiu(void);

#endif
