#ifndef __ESPRESSO_PIC_H
#define __ESPRESSO_PIC_H

unsigned int espresso_pic_get_irq(void);
void espresso_pic_probe(void);

#endif
